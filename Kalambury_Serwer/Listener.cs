﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace Kalambury_Serwer
{
    class Listener
    {
        Socket s;
        public bool Listening { get; set; }
        public int Port { get; set; }
        public Listener(int port)
        {
            Port = port;
            s = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        }
        public void Start(string IP)
        {
            if (Listening)
                return;

            s.Bind(new IPEndPoint(IPAddress.Parse(IP), Port));
            s.Listen(0);

            s.BeginAccept(callback, null);
            Listening = true;
        }
        public void Stop()
        {
            if (!Listening)
                return;

            s.Close();
            s.Dispose();
            s = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

        }
        void callback(IAsyncResult ar)
        {
            try
            {
                Socket s = this.s.EndAccept(ar);
                if (SocketAccepted !=null)
                {
                    SocketAccepted(s);
                }
                this.s.BeginAccept(callback, null);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        public delegate void SocketAcceptHandler(Socket e);
        public event SocketAcceptHandler SocketAccepted;
    }
}
