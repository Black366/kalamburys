﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;
using System.Net.Sockets;
using System.Net;

namespace Kalambury_Serwer
{
    public partial class Form1 : Form
    {
        Listener listener;
        string IP = "";
        public Form1()
        {
            InitializeComponent();
            IPHostEntry host;
            host = Dns.GetHostEntry(Dns.GetHostName());

            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                    IP = ip.ToString();
            }

        }
        public List<string> hasla = new List<string>();
        private List<Client> kolejka = new List<Client>();
        Random rand = new Random();
        bool start = false;
        private Client rysuje;
        public int time = 100;
        private List<string> receiveBuff = new List<string>();
        private char[] zanki = { '@', '#', '$', '%', '^', '&', '*', '(', ')' };
        private void button2_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "TXT|*.txt";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                Encoding enc = Encoding.GetEncoding("Windows-1250");
                string FileName = ofd.FileName;
                string SaveFileName = ofd.SafeFileName;
                StreamReader streamReader = new StreamReader(FileName, enc);

                while (!streamReader.EndOfStream)
                {
                    hasla.Add(streamReader.ReadLine());

                }
                streamReader.Close();
            }
            if (hasla.Count > 0)
                button1.Enabled = true;
            else
                button1.Enabled = false;
        }

        private static byte[] buffer = new byte[1024];
        private List<Socket> clientSocket = new List<Socket>();
        private static List<string> clientNick = new List<String>();
        private static Socket serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

        private void SetupServer()
        {
            int port = 100;
            label1.Text = ("Serwer uruchomiony");
            listener = new Listener(port);
            toolStripStatusLabel4.Text = IP + ":" + port.ToString();
            listener.Start(IP);
            listener.SocketAccepted += new Listener.SocketAcceptHandler(Listener_SocketAccepted);
            Load += new EventHandler(Form1_Load);

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void Listener_SocketAccepted(Socket e)
        {
            Client client = new Client(e);
            client.Received += new Client.ClientReceivedHandler(Client_Received);
            client.Disconnected += new Client.ClientDisconnectedHandler(Client_Disconnected);

            Invoke((MethodInvoker)delegate
            {
                ListViewItem i = new ListViewItem();
                i.Text = client.EndPoint.ToString();
                i.SubItems.Add(client.ID);
                i.SubItems.Add("XX");
                i.SubItems.Add("0");
                i.SubItems.Add("XX");
                i.Tag = client;
                listClients.Items.Add(i);
                toolStripStatusLabel2.Text = listClients.Items.Count.ToString();
            });
        }

        private void Client_Disconnected(Client sender)
        {
            Invoke((MethodInvoker)delegate
            {
                for (int i = 0; i < listClients.Items.Count; i++)
                {
                    Client client = listClients.Items[i].Tag as Client;
                    if (client.ID == sender.ID)
                    {
                        if (client == rysuje)
                        {
                            start = false;
                            timer1.Stop();
                            time = 100;
                        }
                        listClients.Items.RemoveAt(i);
                        Kolejka.Remove(client);
                        toolStripStatusLabel5.Text = kolejka.Count.ToString();

                        break;
                    }
                }
                SendStat();
                toolStripStatusLabel2.Text = listClients.Items.Count.ToString();
            }
            );
        }
        string test = "";
        int gosc = 1;
        
        private void Client_Received(Client sender, byte[] data)
        {
            Invoke((MethodInvoker)delegate
            {
                for (int i = 0; i < listClients.Items.Count; i++)
                {
                    Client client = listClients.Items[i].Tag as Client;
                    if (client.ID == sender.ID)
                    {
                        string s = Encoding.UTF8.GetString(data);
                        listClients.Items[i].SubItems[2].Text = s;

                        string tmp = "";
                        for (int j = 0; j < s.Length; j++)
                        {
                            if (!zanki.Contains(s[j]) || j == 0)
                            {
                                tmp += s[j];
                                if (j == s.Length - 1)
                                {
                                    receiveBuff.Add(tmp);
                                    tmp = "";
                                }
                            }

                            else if (zanki.Contains(s[j]) && j != 0)
                            {
                                receiveBuff.Add(tmp);
                                tmp = "";
                                tmp += s[j];
                            }
                            else
                            {
                                receiveBuff.Add(tmp);
                                tmp = "";
                            }
                        }
                        foreach (var item in receiveBuff)
                        {
                            s = item;
                            if (s[0] == '#')
                            {
                                string sTmp = s.Replace("#", "");
                                string sTmp2 = sTmp.Replace("\r\n", "").ToLower();
                                textBox1.Text += client.nick + ": " + sTmp;
                                if (sTmp2 != "" && toolStripStatusLabel7.Text.ToLower() == sTmp2)
                                {
                                    if (client == rysuje)
                                    {
                                        client.sck.Send(Encoding.UTF8.GetBytes("#Nie podpowiadaj!!" + "\r\n"));
                                    }
                                    else
                                    {
                                        client.punkty++;
                                        rysuje.punkty++;
                                        SendAll("$stop", -1);
                                        start = false;
                                        timer1.Stop();
                                        time = 100;
                                        Task.Delay(2);
                                        SendAll("$clear", -1);
                                        Task.Delay(2);
                                        SendAll("#" + "S:Brawo " + client.nick + " poprawna odpowiedz to:\r\n" + toolStripStatusLabel7.Text + "\r\n", -1);
                                        Task.Delay(5);
                                        SendStat();
                                    }
                                }
                                else if (StringCompare(sTmp2, toolStripStatusLabel7.Text) > 80 && StringCompare(sTmp2, toolStripStatusLabel7.Text) < 100)
                                {
                                    if (client == rysuje)
                                    {
                                        client.sck.Send(Encoding.UTF8.GetBytes("#Nie podpowiadaj!!" + "\r\n"));
                                    }
                                    else
                                    {
                                        SendAll("#" + "S:Blisko " + client.nick + " zgadza się: " + StringCompare(sTmp2, toolStripStatusLabel7.Text).ToString() + "%" + "\r\n", -1);
                                    }
                                }
                                else
                                    SendAll("#" + client.nick + ": " + sTmp, -1);
                            }
                            else if (s[0] == '@')
                            {
                                string sTmp = s.Replace("@", "");

                                if (sTmp != "")
                                    client.nick = sTmp;
                                else
                                {
                                    client.nick = "Gośc " + gosc++.ToString();
                                }
                                listClients.Items[i].SubItems[4].Text = client.nick;
                                SendAll("#Do gry dolaczyl gracz: " + client.nick + "\r\n", -1);
                                client.punkty = 0;
                                SendStat();
                                //textBox1.Text += sTmp;
                            }
                            else if (s[0] == '$')
                            {
                                string sTmp = s.Replace("$", "");
                                if (sTmp == "r")
                                {
                                    Kolejka.Add(client);
                                    toolStripStatusLabel5.Text = kolejka.Count.ToString();
                                }
                                if (sTmp == "res")
                                {
                                    SendAll("$stop", -1);
                                    start = false;
                                    timer1.Stop();
                                    time = 100;
                                    Task.Delay(2);
                                    SendAll("$clear", -1);
                                }
                                if (sTmp == "clear")
                                    SendAll(s, i);
                                if (sTmp == "StopMyBrush")
                                    SendAll(s, i);


                            }
                            else if (s[0] == '(')
                            {
                                SendAll(s, i);
                            }
                            else if (s[0] == ')')
                            {
                                SendAll(s, i);
                            }
                            else if (s[0] == '%')
                            {
                                SendAll(s, i);
                                //test += s;
                            }
                            else
                                SendAll(s, i);
                            //listClients.Items[i].SubItems[3].Text = DateTime.Now.ToString();
                            //break;
                        }
                    }
                    receiveBuff.Clear();
                }

                Gra();
            }
            );
        }
        private void Gra()
        {
            toolStripStatusLabel6.Text = start.ToString();
            toolStripStatusLabel2.Text = listClients.Items.Count.ToString();
            if (listClients.Items.Count >= 2 && kolejka.Count > 0 && start == false)
            {
                start = true;
                timer1.Start();
                Task.Delay(5);
                SendAll("$start", -1);
                Task.Delay(2);
                int r = rand.Next(hasla.Count);
                int s = kolejka[0].sck.Send(Encoding.UTF8.GetBytes("^" + hasla[r]));
                rysuje = kolejka[0];
                kolejka.RemoveAt(0);
                toolStripStatusLabel7.Text = hasla[r];
                hasla.RemoveAt(r);
            }
        }
        private void SendStat()
        {
            string data = "";
            for (int i = 0; i < listClients.Items.Count; i++)
            {

                Client client = listClients.Items[i].Tag as Client;
                data += client.nick + "\t" + client.punkty + "\r\n";
                listClients.Items[i].SubItems[3].Text = client.punkty.ToString();
            }
            for (int i = 0; i < listClients.Items.Count; i++)
            {

                Client client = listClients.Items[i].Tag as Client;
                int s = client.sck.Send(Encoding.UTF8.GetBytes("*" + data));
            }
        }
        private void SendAll(string data, int a)
        {
            try
            {
                for (int i = 0; i < listClients.Items.Count; i++)
                {
                    if ((i == a && data[0] == '%') || data[0] == '@')
                        continue;
                    Client client = listClients.Items[i].Tag as Client;
                    int s = client.sck.Send(Encoding.UTF8.GetBytes(data));
                }
            }
            catch
            {

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SetupServer();
            button1.Enabled = false;

        }

        private void textBox2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && textBox2.Text != "")
            {
                textBox1.Text += textBox2.Text;
                SendAll('#' + textBox2.Text, 0);
                textBox2.Clear();
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            textBox1.SelectionStart = textBox1.Text.Length;
            textBox1.ScrollToCaret();
        }
        SaveFileDialog sfd = new SaveFileDialog();

        internal List<Client> Kolejka
        {
            get
            {
                return kolejka;
            }

            set
            {
                kolejka = value;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            sfd.Filter = "TXT|*.txt";
            sfd.FileName = "Rozkodowany_tekst";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                StreamWriter streamWriter = new StreamWriter(sfd.FileName);
                streamWriter.Write(test);
                MessageBox.Show("Zapisano", "Informacja",
                                 MessageBoxButtons.OK,
                                 MessageBoxIcon.Information);
            }
        }
        double StringCompare(string a, string b)
        {
            if (a == b) //Same string, no iteration needed.
                return 100;
            if ((a.Length == 0) || (b.Length == 0)) //One is empty, second is not
            {
                return 0;
            }
            double maxLen = a.Length > b.Length ? a.Length : b.Length;
            int minLen = a.Length < b.Length ? a.Length : b.Length;
            int sameCharAtIndex = 0;
            for (int i = 0; i < minLen; i++) //Compare char by char
            {
                if (a[i] == b[i])
                {
                    sameCharAtIndex++;
                }
            }
            return sameCharAtIndex / maxLen * 100;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            SendAll("+" + time--, -1);
            if (time < 0)
            {
                start = false;
                time = 100;
                SendAll("$stop", -1);
                Task.Delay(2);
                SendAll("$clear", -1);

                timer1.Stop();
                Gra();
            }
            Task.Delay(2);
        }
    }
}
